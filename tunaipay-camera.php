<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0;">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Muridigital - Payment</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">
    <link rel="icon" href="http://www.muridigital.com/assets/images/muridigital.png" sizes="32x32" />
    <link rel="icon" href="http://www.muridigital.com/assets/images/muridigital.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="http://www.muridigital.com/assets/images/muridigital.png" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/html5-qrcode"></script>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <link href="style.css" rel="stylesheet">
</head>

<body class="text-center">
    <div class="col-12">
        <div class="row">
            <form class="form-signin justify-content-center" action="home/checkout" method="POST">
                <div id="qr-reader" style="width:100%;align-content: center!important"></div>
            </form>
        </div>
    </div>
</body>
<script type="text/javascript">
    const qrCodeSuccessCallback = (decodedText, decodedResult) => {
        /* Redirect success */
        // window.location.replace(decodedText);

        /* Result success */
        alert(decodedText);
    };
    const html5QrCode = new Html5Qrcode("qr-reader");
    const config = {
        fps: 10,
        qrbox: {
            width: 250,
            height: 250
        }
    };
    html5QrCode.start({
        facingMode: "environment"
    }, config, qrCodeSuccessCallback);
</script>

</html>